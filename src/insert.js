import React from 'react';
import './index.css';


function Clicked(props) {
  const isClicked = props.isClicked;
  const name = props.name;
  if (isClicked===1) {
    return <h1><del>{name}</del></h1>
  }
  else if (isClicked===0) {
    return <h1>{name}</h1>
  }
  else {
    return <h40></h40>
  }
}



class Insert extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      count1: 0
    };
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick() {
    this.setState(prevState => ({
      count1: ++ prevState.count1,
    }));
  }
  render() {
    return (
      <div >
        <button onClick={() =>{this.handleClick()}}><Clicked isClicked={this.state.count1} name={this.props.name} /></button>
      </div>

    );
  }
}

export default Insert;
