
import React from 'react';
import './index.css';
import Insert from './insert';

function ListItem(props) {
  return <Insert name={props.value} />;
}


function NumberList(props) {
  const numbers = props.numbers;
  const listItems = numbers.map((number) =>
    <ListItem key={number.toString()}
              value={number} />
  );
  return (
    <ul>
      {listItems}
    </ul>
  );
}


class Game extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      numbers:[],
      value: ''
    };
  }
  handleSubmitClick = () => {
    const name = this._name.value;
    this.setState(prevState => ({
      numbers:[...prevState.numbers, ...[name]]
    }));
  }
  render() {
    return(
      <div >
        <div>
                <input type="text" ref={input => this._name = input} />
                <button onClick={this.handleSubmitClick}>ADD something in todo list</button>
        </div>
        <NumberList numbers={this.state.numbers} />
      </div>
    );
  }
}

export default Game;
